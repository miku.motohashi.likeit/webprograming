

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import dao.DetailDao;
import dao.UpdateDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//id受け取る,daoで検索、取得して、jspに表示
				String loginId = request.getParameter("id");
				
				DetailDao detailDao = new DetailDao();
				User user = detailDao.findByDetailInfo(loginId);
				// セッションにユーザの情報をセット
						request.setAttribute("user", user);
						
						RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userInfoUpdate.jsp");
						dispatcher.forward(request, response);
			}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password=request.getParameter("password");
		String rePassword=request.getParameter("rePassword");
	
		
		UpdateDao updateDao = new UpdateDao();
		try {
			if (!(password.equals(rePassword)) || loginId.equals("") || name.equals("")|| birthDate.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				// 

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userInfoUpdate.jsp");
				dispatcher.forward(request, response);
				return;
			}
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";
			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			String encryptPassword = DatatypeConverter.printHexBinary(bytes);
			
			
			
			updateDao.UpdateInfo(loginId,name,birthDate,encryptPassword,rePassword);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
				
		response.sendRedirect("UserListServlet");

		
		
	}
	}


