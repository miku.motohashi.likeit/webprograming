
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import dao.ResisterDao;
import model.User;

/**
 * Servlet implementation class ResistServlet
 */
@WebServlet("/ResistServlet")
public class ResistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ResistServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userResister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定 
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行		・・・・①
		ResisterDao resisterDao = new ResisterDao();
		try {
			if (loginId.equals("") || password.equals("") || rePassword.equals("") || name.equals("")
					|| birthDate.equals("") || !(password.equals(rePassword)) ){
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				// 

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userResister.jsp");
				dispatcher.forward(request, response);
				return;

			} 
			

			//daoで検索、ヒットしたら登録済→エラー　ヒットしない→登録 loginId
			User user =resisterDao.findByUserInfo(loginId);
			if(user!=null) {
				//ヒットした
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				// 

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userResister.jsp");
				dispatcher.forward(request, response);
				return;

			}
			//ハッシュを生成したい元の文字列
			
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";
			//ハッシュ生成処理
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
			String encryptPassword = DatatypeConverter.printHexBinary(bytes);
			
			resisterDao.resistInfo(loginId, encryptPassword, name, birthDate);
			
			
			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserListServlet");

		} catch (ServletException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}
}
