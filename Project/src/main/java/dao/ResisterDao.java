package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;

import model.User;

public class ResisterDao {
	public void resistInfo(String loginId, String encryptPassword, String name, String birthDate) throws ServletException, SQLException {
		Connection conn = null;
			// データベースへ接続
			conn = DBManager.getConnection();
			try {
			
			String sql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date) VALUES(?,?,?,?,now(),now())";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, loginId);
			pstmt.setString(2, name);
			pstmt.setString(3, birthDate);
			pstmt.setString(4, encryptPassword);
			
			pstmt.executeUpdate();
			}catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}

				}
			}
			
		
	}	
	//検索メゾット
	public User findByUserInfo(String loginId) throws ServletException, SQLException{
		Connection conn = null;
		// データベースへ接続
		conn = DBManager.getConnection();
		String sql="SELECT * FROM user WHERE login_id=?";
		try{
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();
			
		if (!rs.next()) {
			return null;
		}
			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			User user = new User(id, loginIdData, name, birthDate, password, createDate, updateDate);
			
			return user;
        

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

}
}
				
		return null;
		
	}
}

