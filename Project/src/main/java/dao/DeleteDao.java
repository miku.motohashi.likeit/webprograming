package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;

import model.User;



public class DeleteDao {
	public User deleteInfo(String loginId) throws ServletException, SQLException {
		Connection conn = null;
			// データベースへ接続
			conn = DBManager.getConnection();
			try {
			
			String sql = "DELETE FROM user WHERE login_id=?";
			
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			pStmt.executeUpdate();
			}catch (SQLException e) {
				e.printStackTrace();
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}

				}
			}
			return null;
			
		
	}	

}
