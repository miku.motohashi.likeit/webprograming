package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;

public class UpdateDao {
	public void UpdateInfo(String loginId, String name, String birthDate,String password, String rePassword) throws ServletException, SQLException {
		
		Connection conn = null;
		// データベースへ接続
		conn = DBManager.getConnection();
		try {
		
		String sql = "UPDATE user SET password=?, name=?, birth_date=? WHERE login_Id=?";
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, password);
		pstmt.setString(2, name);
		pstmt.setString(3, birthDate);
		pstmt.setString(4, loginId);
		
		pstmt.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
	}
}
		
		
