package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public class DetailDao {
	public User findByDetailInfo(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			//確認済みのSQL
			String sql = "SELECT * FROM user WHERE id = ?";

			//SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
						if (!rs.next()) {
							return null;
						}
						int id = rs.getInt("id");
		                String loginIdData = rs.getString("login_id");
		                String name = rs.getString("name");
		                Date birthDate = rs.getDate("birth_date");
		                String password = rs.getString("password");
		                String createDate = rs.getString("create_date");
		                String updateDate = rs.getString("update_date");
		                User user = new User(id, loginIdData, name, birthDate, password, createDate, updateDate);
		                return user;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		}
		return null;
	}

	


}
