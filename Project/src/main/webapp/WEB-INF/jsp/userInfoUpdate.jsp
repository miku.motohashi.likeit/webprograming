<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>title</title>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand" class="text-black-50">${userInfo.name}さん</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="text-danger">
				<a class="navi-item" href="LogoutServlet" role="buttun">ログアウト</a>
			</div>
		</div>
	</nav>
	<div class="container">
		<h1>ユーザー情報更新</h1>
		<c:if test="${errMsg != null}">
			<div class="text-center">
				<a class="text-danger"> ${errMsg} </a>
			</div>
		</c:if>

		<form class="form-signin" action="UpdateServlet" method="post">
			<div class="form-group row">
				<label for="inputLoginID" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<div class="pt-2">
						<p>${user.loginId}</p>
						<input type="hidden" class="form-control" id="inputLoginId"
							name="loginId" value="${user.loginId}">
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPasswprd" class="col-sm-2 col-form-label">パスワード</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="inputPassword"
						name="password" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPasswprd" class="col-sm-2 col-form-label">パスワード(確認)</label>
				<div class="col-sm-10">
					<input type="password" class="form-control" id="inputRePassword"
						name="rePassword" placeholder="password">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputUserName" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="inputName" name="name"
						value="${user.name}">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputBirthday" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="inputBirthDate"
						name="birthDate" value="${user.birthDate}">
				</div>
			</div>



			<div class="text-center">
				<div class="padding-8">

					<button type="submit" class="btn btn-outline-secondary">更新</button>

				</div>
			</div>
		</form>

		<br></br>
		<a href="UserListServlet" id="cancel" class="btn btn-link">戻る</a>


	</div>
</body>