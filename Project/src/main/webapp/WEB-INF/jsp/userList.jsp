<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <title>title</title>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" class="text-black-50">${userInfo.name}さん</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                    data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="text-danger">
                <a class="navi-item"  href="LogoutServlet" role="buttun">ログアウト</a>
				</div>
            </div>
        </nav>
        <div class="col-12 col-offset-10">
            <div class="container">
                <h1>ユーザー一覧</h1>
                <div class="float-right">
                     <a class="btn btn-link" href="ResistServlet" role="button">新規登録</a>
                </div>
                <br></br>
 <form method="post" action="UserListServlet" class="form-horizontal">

                <div class="form-group row">
                    <label for="inputLoginID" class="col-sm-2 col-form-label">ログインID</label>
                    <div class="col-sm-10">
                        <input type="loginId" name="loginId" class="form-control" id="inputLoginID" placeholder="LoginID">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputUserName"  class="col-sm-2 col-form-label">ユーザー名</label>
                    <div class="col-sm-10">
                        <input type="userName" name="name" class="form-control" id="inputUserName" placeholder="UserName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputBirthday" class="col-sm-2 col-form-label">生年月日</label>


                    <div class="col-auto">
                        <input type="birthday" name="birthDate" class="form-control" id="inputbirthday" placeholder="年/月/日">
                    </div>
                    <p>　〜　</p>
                    <div class="col-auto">
                        <input type="birthday" name="birthDate2" class="form-control" id="inputbirthday" placeholder="年/月/日">
                    </div>
                </div>




                <div class="float-right">
                    <div class="form-group row">
                        <button type="submit" class="btn btn-outline-secondary">検索</button>
                    </div>
                </div>
                </form>
                <br></br>
                <div class="row border-bottom"></div>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">ログインID</th>
                                <th scope="col">ユーザー名</th>
                                <th scope="col">生年月日</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>

                        <tbody>
                   <c:forEach var="user" items="${userList}">
                          
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                      <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                     	<c:if test="${userInfo.loginId=='admin'}">
                     		
                        	<a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
                       		<a class="btn btn-danger" href ="DeleteServlet?id=${user.id}">削除</a>
                     	</c:if>
                     	<c:if test="${userInfo.loginId==user.loginId}">
                        	<a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
                     	</c:if>
                     	
                     </td>
                   </tr>
                 </c:forEach>
                          
                            
                        </tbody>

                    </table>
                </div>
            </div>

        </div>
    </body>

    </html>