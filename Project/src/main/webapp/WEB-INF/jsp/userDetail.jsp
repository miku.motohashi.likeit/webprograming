<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>title</title>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<div class="container-fluid">
			<a class="navbar-brand" class="text-black-50">${userInfo.name}さん</a>
			<button class="navbar-toggler" type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="text-danger">
				<a class="navi-item" href="LogoutServlet" role="buttun">ログアウト</a>
			</div>

		</div>
	</nav>

	<div class="container">
		<h1>ユーザー情報詳細参照</h1>

		<style>
body {
	margin: 0;
}

.list {
	list-style-type: none;
	padding: 0;
	margin: 0;
}

.item {
	margin: 5px;
	float: left;
	width: calc(50% - 10px);
}
</style>
		<div class="list">
			<li class="item">ログインID</li>
			<li class="item">${user.loginId}</li>

			<li class="item">ユーザー名</li>
			<li class="item">${user.name}</li>

			<li class="item">生年月日</li>
			<li class="item">${user.birthDate}</li>

			<li class="item">登録日時</li>
			<li class="item">${user.createDate}</li>

			<li class="item">更新日時</li>
			<li class="item">${user.updateDate}</li>
		</div>

		<br></br>

		<a href="UserListServlet" id="cancel" class="btn btn-link">戻る</a>
		


	</div>

</body>

</html>